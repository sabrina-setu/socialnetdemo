# SocialNetDemo

Demonstration of Social Network Functionalities, such as adding friend, send friend requests, like, comments, create post, see timeline posts, chat etc. This project is a requirement of partial fulfillment of Training on Web App Development Dot Net in BITM.

## Getting Started

To test the application just download full repository, open in Visual Studio 2015 or later and run.

### Prerequisites

To run this application you need
* .NET 4.5
* SQL Server 2012 or later
* Visual Studio 2015 or later
in your Computer.

## Built With

* ASP.NET MVC 5
* Entity Framework 6
* LINQ
* Visual C#
* SignalR Library
* jQuery
* Ajax
* Razor View Engine
* HTML5
* CSS
* SQL Server 2014
* .NET Framework 4.5
* Visual Studio 2015

##