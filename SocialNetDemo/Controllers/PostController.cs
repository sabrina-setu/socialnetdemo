﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SocialNetDemo.Models;

namespace SocialNetDemo.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private ApplicationDbContext postDb = new ApplicationDbContext();
        // GET: Post
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Post post)
        {
            post.ApplicationUserId = User.Identity.GetUserId();
            post.PostDateTime = DateTime.Now;
            try
            {
                postDb.Posts.Add(post);
                postDb.SaveChanges();
                return RedirectToAction("ViewAllPost");
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
                return View();
            }

        }

        public ActionResult ViewAllPost()
        {
            string id = User.Identity.GetUserId();
            try
            {
                List<Post> posts = postDb.Posts.Where(p => p.ApplicationUserId.Equals(id)).ToList();
                return View(posts);
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Post aPost = new Post() { Id = id };
                postDb.Posts.Attach(aPost);
                postDb.Posts.Remove(aPost);
                postDb.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("ViewAllPost");
        }

        public ActionResult TimeLine()
        {
            List<Post> timeLinePosts = new List<Post>();

            try
            {
                string thisUserId = User.Identity.GetUserId();
                ApplicationUser thisUser = postDb.Users.FirstOrDefault(u => u.Id.Equals(thisUserId));
                var friends = thisUser.Friends.ToList();
                friends.AddRange(thisUser.FriendOf.ToList());
                foreach (ApplicationUser friend in friends)
                {
                    timeLinePosts.AddRange(postDb.Posts.Where(p => p.ApplicationUserId.Equals(friend.Id)));
                }
                timeLinePosts.AddRange(postDb.Posts.Where(p => p.ApplicationUserId.Equals(thisUserId)));
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
                throw;
            }
            timeLinePosts=timeLinePosts.OrderByDescending(o => o.PostDateTime).ToList();
            return View(timeLinePosts);
        }

        public ActionResult LikeComment(int? likeBtn, string commentText, int? commentBtn)
        {
            string userId = User.Identity.GetUserId();
            if (likeBtn != null)
            {
                try
                {
                    Like chkLike = postDb.Likes.FirstOrDefault(
                        l => l.ApplicationUserId == userId && l.PostId == likeBtn);
                    if(chkLike==null)
                    {
                        var like = new Like() { ApplicationUserId = userId, PostId = Convert.ToInt32(likeBtn) };
                        postDb.Likes.AddOrUpdate(like);
                        postDb.SaveChanges();
                    }
                }
                catch (DbEntityValidationException e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            if (commentBtn != null && !string.IsNullOrEmpty(commentText))
            {
                try
                {
                    var comment = new Comment() { ApplicationUserId = userId, PostId = Convert.ToInt32(commentBtn), CommentText = commentText};
                    postDb.Comments.Add(comment);
                    postDb.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return RedirectToAction("Timeline");
        }
    }
}