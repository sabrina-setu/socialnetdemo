﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SocialNetDemo.Models;

namespace SocialNetDemo.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        ApplicationDbContext _context = ApplicationDbContext.Create();
        // GET: Chat
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            ViewBag.User = user;
            return View();
        }
    }
}