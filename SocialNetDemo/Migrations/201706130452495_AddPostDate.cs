namespace SocialNetDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "PostDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "PostDateTime");
        }
    }
}
